const express = require("express");
const router = express.Router();

// connexion à la base de données pour faire les requêtes SQL
const connection = require("./conf/db");

/*Routes pour le CRUD */
//on remplace app par router

//route GET pour récupérer toutes les voitures de la base de données
//on supprime l'occurrence /cars (déjà définie lors de l'utilisation du router dans index.js ligne 29)
router.get('/', (req, res) => {
  // Exécution d'une requête SQL pour sélectionner toutes les voitures
  connection.query('SELECT * FROM car', (err, results) => {
    // Gestion d'erreurs éventuelles lors de l'exécution de la requête
    if (err) {
      res.status(500).send('Erreur lors de la récupération des voitures');
    } else {
      // Envoi des résultats sous forme de JSON si la requête est réussie
      res.json(results);
    }
  });
});

// Route GET pour récupérer une voiture spécifique par son ID en utilisant une requête préparée pour éviter les injections SQL
router.get('/:id', (req, res) => {
  const id = req.params.id;
  // Utilisation d'une requête préparée avec un paramètre pour sécuriser l'accès aux données
  connection.execute('SELECT * FROM car WHERE car_id = ?', [id], (err, results) => {
    if (err) {
      res.status(500).send('Erreur lors de la récupération de la voiture');
    } else {
      // Envoi du résultat sous forme de JSON si la requête est réussie
      res.json(results);
    }
  });
});

// Route POST pour ajouter une nouvelle voiture dans la base de données
router.post('/', (req, res) => {
  const { brand, model } = req.body;
  // Exécution d'une requête SQL pour insérer une voiture
  connection.execute(
    "INSERT INTO car (brand, model) VALUES (?, ?)",
    [brand, model],
    (err, results) => {
      if (err) {
        res.status(500).send("Erreur lors de l'ajout de la voiture");
      } else {
        // Confirmation de l'ajout dela voiture avec l'ID de la voiture insérée
        res.status(201).send(`Voiture ajoutée avec l'ID ${results.insertId}`);
      }
    }
  );
});

// Route PUT pour mettre à jour une voiture existante par son ID
router.put('/:id', (req, res) => {
  const { brand, model } = req.body;
  const id = req.params.id;
  // Exécution d'une requête SQL pour mettre à jour les informations de la voiture spécifiée
  connection.execute(
    "UPDATE car SET brand = ?, model = ? WHERE car_id = ?",
    [brand, model, id],
    (err, result) => {
      if (err) {
        res.status(500).send("Erreur lors de la mise à jour de la voiture");
      } else {
        // Confirmation de la mise à jour de la voiture
        res.send(`Voiture mise à jour.`);
      }
    }
  );
});

// Route DELETE pour supprimer une voiture par son ID
router.delete('/:id', (req, res) => {
    //recherche avec le paramètre de l'url :id
  const id = req.params.id;
  // Exécution d'une requête SQL pour supprimer la voiture spécifié
  connection.execute('DELETE FROM car WHERE car_id = ?', [id], (err, results) => {
    if (err) {
      res.status(500).send('Erreur lors de la suppression de la voiture');
    } else {
      // Confirmation de la suppression de la voiture
      res.send(`Voiture supprimée.`);
    }
  });
});

// Route GET pour récupérer les voitures d'une marque
router.get('/brand/:brand', (req, res) => {
  //recherche avec query pour récupérer le paramètre de requête et non le paramètre de l'url
  const brand = req.params.brand;
  // Utilisation d'une requête préparée avec un paramètre pour sécuriser l'accès aux données
  connection.execute(
    "SELECT * FROM car WHERE brand = ?",
    [brand],
    (err, results) => {
      if (err) {
        res.status(500).send("Erreur lors de la récupération de la marque");
      } else {
        // Envoi du résultat sous forme de JSON si la requête est réussie
        res.json(results);
      }
    }
  );
});

module.exports = router;
