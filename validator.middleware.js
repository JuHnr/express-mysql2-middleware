const emailRegex = /^[^\s@]+@[^\s@]+\.[^\s@]+$/; //email au format xxxx @ xxxx .xxx

// fonction
const validateGarageData = (req, res, next) => {
  const { name, email } = req.body;

  // Vérifier que le nom et l'email sont présents dans la requête (erreur 400 : Bad Request)
  if (!name || !email) {
    return res
      .status(400)
      .json({ message: "Le nom et l'email sont obligatoires." });

    //vérifie que l'email est valide
  } else if (!validateEmail(email)) {
    return res.status(400).json({ message: "Adresse e-mail non valide." });
  }

  //fonction pour valider le format de l'email
  function validateEmail(email) {
    return emailRegex.test(email);
  }

  // passe au middleware suivant
  next();
};

module.exports = { validateGarageData };
